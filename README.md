# Skill Test - Flutter - PT Infosys Solusi Terpadu

Project ini merupakan skill test dari [PT Infosys Solusi Terpadu](https://ist.id/) untuk posisi **Flutter Developer**.

## Screenshot

* Android
![android](https://user-images.githubusercontent.com/56527536/157260969-52907cbc-bbee-4f4b-af76-0e430a155bfa.png)

* iOS
![ios](https://user-images.githubusercontent.com/56527536/157261281-912a1cde-ed74-44fa-8f0c-0f2e540f7adb.png)

## Ada yang ingin di diskusikan? Hubungi saya melalui

* [LinkedIn](https://www.linkedin.com/in/nazarudin/)
* [WhatsApp](https://wa.me/6281365041803)
