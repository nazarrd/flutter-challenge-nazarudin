import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import 'app/modules/login/controllers/login_controller.dart';
import 'app/modules/login/views/login_view.dart';
import 'app/routes/app_pages.dart';
import 'app/widgets/splash.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        final splash = Get.put(LoginController()).splash.value;
        return GetMaterialApp(
          title: "Flutter Challenge",
          debugShowCheckedModeBanner: false,
          getPages: AppPages.routes,
          defaultTransition: Transition.native,
          home: ResponsiveSizer(
            builder: (context, orientation, deviceType) {
              return splash ? const Splash() : const LoginView();
            },
          ),
        );
      },
    );
  }
}
