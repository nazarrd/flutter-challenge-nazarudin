import 'package:flutter/material.dart';

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image.asset('assets/images/header-splash.png'),
            Image.asset('assets/images/logo.png'),
            Image.asset('assets/images/footer-splash.png'),
          ],
        ),
      ),
    );
  }
}
