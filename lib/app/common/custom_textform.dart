import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomInput extends StatelessWidget {
  const CustomInput({
    Key? key,
    required this.label,
    required this.hintText,
    required this.ctr,
    required this.controller,
    this.obsecureText,
    this.textInputAction,
    this.suffixIcon,
  }) : super(key: key);
  final String label, hintText;
  final TextEditingController ctr;
  final GetxController controller;
  final bool? obsecureText;
  final TextInputAction? textInputAction;
  final Widget? suffixIcon;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label, style: GoogleFonts.roboto(fontSize: 15)),
        TextFormField(
          controller: ctr,
          textAlignVertical: TextAlignVertical.bottom,
          textInputAction: textInputAction ?? TextInputAction.next,
          onChanged: (value) => controller.update(),
          obscureText: obsecureText ?? false,
          decoration: InputDecoration(
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.grey),
            ),
            isDense: true,
            hintText: hintText,
            hintStyle: GoogleFonts.roboto(
              color: Colors.grey,
              fontSize: 14,
              fontStyle: FontStyle.italic,
            ),
            suffixIconConstraints: const BoxConstraints(maxHeight: 15),
            suffixIcon: suffixIcon ?? const SizedBox(),
          ),
        ),
      ],
    );
  }
}
