import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controller/global_controller.dart';

class LoginController extends GetxController {
  var splash = true.obs, hidePass = true.obs;
  late TextEditingController userC, passC;

  @override
  void onInit() {
    Timer(const Duration(seconds: 2), () => splash.value = false);
    userC = TextEditingController();
    passC = TextEditingController();
    super.onInit();
  }

  @override
  void onClose() {
    userC.dispose();
    passC.dispose();
    super.onClose();
  }

  login() async {
    final global = Get.put(GlobalController());
    if (userC.text == '' || passC.text == '') {
      await global.alerDialog(
        title: 'LOGIN GAGAL',
        description:
            'User ID dan atau Password anda belum diisi, periksa semua form input lalu coba lagi.',
      );
    } else {
      await global.alerDialog(
        title: 'LOGIN BERHASIL',
        description: 'Login berhasil, silahkan tutup dialog ini.',
      );
    }
    Get.delete<GlobalController>();
  }
}
