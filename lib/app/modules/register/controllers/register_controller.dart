import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../controller/global_controller.dart';

class RegisterController extends GetxController {
  var hidePass = true.obs;
  late TextEditingController userC, nameC, passC;

  @override
  void onInit() {
    userC = TextEditingController();
    nameC = TextEditingController();
    passC = TextEditingController();
    super.onInit();
  }

  @override
  void onClose() {
    userC.dispose();
    nameC.dispose();
    passC.dispose();
    super.onClose();
  }

  register() async {
    final global = Get.put(GlobalController());
    if (userC.text == '' || passC.text == '' || userC.text == '') {
      await global.alerDialog(
        title: 'REGISTER GAGAL',
        description:
            'User ID, Nama dan atau Password anda belum diisi, periksa semua form input lalu coba lagi.',
      );
    } else {
      await global
          .alerDialog(
            title: 'REGISTER BERHASIL',
            description:
                'Register berhasil, silahkan lakukan login dengan akun Anda.',
          )
          .then((value) => Get.back());
    }
    Get.delete<GlobalController>();
  }
}
