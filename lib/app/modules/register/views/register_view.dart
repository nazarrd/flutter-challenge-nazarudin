import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../../common/custom_textform.dart';
import '../controllers/register_controller.dart';

class RegisterView extends StatelessWidget {
  const RegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterController>(
      init: RegisterController(),
      builder: (_) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset('assets/images/header-login.png'),
                    Padding(
                      padding: const EdgeInsets.only(top: 75, left: 10),
                      child: Image.asset('assets/images/logo.png', width: 100),
                    ),
                  ],
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Register',
                        style: GoogleFonts.roboto(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 0.75.h),
                      Text(
                        'Please fill all the form below',
                        style: GoogleFonts.roboto(fontSize: 15),
                      ),
                      SizedBox(height: 3.h),
                      CustomInput(
                        label: 'User ID',
                        hintText: 'User ID',
                        ctr: _.userC,
                        controller: _,
                      ),
                      SizedBox(height: 3.h),
                      CustomInput(
                        label: 'Name',
                        hintText: 'Name',
                        ctr: _.nameC,
                        controller: _,
                      ),
                      SizedBox(height: 3.h),
                      CustomInput(
                        label: 'Password',
                        hintText: 'Password',
                        ctr: _.passC,
                        controller: _,
                        obsecureText: _.hidePass.value,
                        textInputAction: TextInputAction.done,
                        suffixIcon: Container(
                          width: 5.w,
                          padding: const EdgeInsets.only(bottom: 5, right: 5),
                          child: GestureDetector(
                            onTap: () {
                              _.hidePass.toggle();
                              _.update();
                            },
                            child: Icon(
                              _.hidePass.value
                                  ? Icons.visibility_off
                                  : Icons.visibility,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 2.h),
                      Align(
                        alignment: Alignment.centerRight,
                        child: ElevatedButton(
                          onPressed: () => _.register(),
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25),
                            ),
                            side: const BorderSide(color: Colors.white),
                            padding: const EdgeInsets.fromLTRB(40, 13, 40, 13),
                            primary: const Color(0XFF6338A0),
                            onPrimary: Colors.blue,
                            shadowColor: Colors.transparent,
                          ),
                          child: Text(
                            'REGISTER',
                            style: GoogleFonts.roboto(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const Spacer(),
                Center(
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: const TextStyle(
                        color: Colors.grey,
                        fontWeight: FontWeight.w600,
                      ),
                      children: <TextSpan>[
                        const TextSpan(text: 'Already have an account? '),
                        TextSpan(
                          text: 'Sign In',
                          style: const TextStyle(
                            color: Color(0XFFED4B25),
                            fontWeight: FontWeight.w600,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => Get.back(),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 3.h),
              ],
            ),
          ),
        );
      },
    );
  }
}
