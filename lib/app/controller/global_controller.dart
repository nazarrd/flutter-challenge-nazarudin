import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

class GlobalController extends GetxController {
  Future alerDialog({String? title, String? description}) async {
    await Get.defaultDialog(
      title: '$title',
      titlePadding: const EdgeInsets.only(top: 20, bottom: 15),
      titleStyle: GoogleFonts.roboto(fontWeight: FontWeight.bold, fontSize: 18),
      content: Text(
        '$description',
        textAlign: TextAlign.justify,
        style: GoogleFonts.roboto(fontSize: 15),
      ),
      contentPadding: const EdgeInsets.symmetric(horizontal: 20),
      actions: [
        Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  behavior: HitTestBehavior.opaque,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: const Color(0XFFED4B25),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 5.0,
                        horizontal: 10.0,
                      ),
                      child: Text(
                        'OK',
                        style: GoogleFonts.roboto(
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
